# Group: 8-bit Federation (Team 8)
# Muaath Alaraj	
# Christopher Caldwell
# Doan Dinh	
# Corey Johnson	
# Thomas Krause	
# Date: 4/3/2018
# LAB 11
# VERSION 1.01

# Classes to make configuration of the map and items easier
class Location:
    def __init__(self, x=0, y=0):
        self.x = x
        self.y = y


class Room:
    def __init__(self, name, message):
        self.name = name
        self.message = message
        self.items = []

    def addItem(self, item):
        return self.addItems([item])

    def addItems(self, items):
        self.items.extend(items)
        return self


class Item:
    def __init__(self, name, description):
        self.name = name
        self.description = description


# Items
sword = Item('sword', 'An ancient sword rusty sword')
bow = Item('bow', 'A powerful hunting bow')

# Rooms
forest = Room('Forest', 'You are in a spooky forest')
house = Room('House', 'You are in an old neglected cabin with peeling paint and broken shutters').addItems([sword, bow])

world = [
    [forest, forest, forest, forest],
    [None, house, None, forest],
    [None, forest, None, forest],
]

# Logic
location = Location()
inventory = []


def look():
    room = getCurrent()
    print room.message
    if len(room.items) > 0:
        print 'In the %s you see the items: %s' % (room.name, ', '.join(item.name for item in room.items))

    # gather surrounding data
    for d in ['north', 'south', 'east', 'west']:
        loc = canMove(d[:1])
        if loc is None:
            continue
        print 'To the %s you see a %s' % (d, getCurrent(loc).name)


def getCurrent(loc=None):
    loc = location if loc is None else loc
    return world[loc.y][loc.x]


def canMove(direction):
    # copy the current location
    newLoc = Location(location.x, location.y)

    # just do the move, check later
    if direction == 'n':
        newLoc.y -= 1
    elif direction == 's':
        newLoc.y += 1
    elif direction == 'w':
        newLoc.x -= 1
    else:
        newLoc.x += 1

    # if it's not in the bounds return false
    if newLoc.x < 0 or newLoc.x > len(world[0]) - 1 or newLoc.y < 0 or newLoc.y > len(world) - 1:
        return None

    # check that the map actually has a room to move to
    if not isinstance(getCurrent(newLoc), Room):
        return None

    # all checks passed return the new location
    return newLoc


def move(direction):
    # the function needs to be able to update the location
    global location
    # see if we can move in the direction requested
    loc = canMove(direction)
    if loc is None:
        print 'After seeing the edge of the earth and your life flashes before your eyes, you decided not to move...'
        return
    # commit the actual move
    location = loc


def take(name):
    global inventory

    if not name:
        print 'You must pick an item to take nothing selected'
        return

    room = getCurrent()
    for i, item in enumerate(room.items):
        if item.name == name:
            # add item to the inventory and remove from the room
            inventory.append(room.items.pop(i))

            print 'You snatched the %s and put it in your bag' % item.name
            return

    print 'There is no %s in the %s' % (name, room.name)


def bag():
    if len(inventory) > 0:
        print 'In your bag, you have: %s' % (', '.join(item.name for item in inventory))
    else:
        print 'Your bag is empty'

def help():
    welcome()
    print 'List of commands: '
    print 's or south - to go south'
    print 'w or west - to go west'
    print 'n or north - to go north'
    print 'e or east - to go east'
    print 'look - to display a description of the room'
    print 'take {item name} - to take item in room'
    print 'kill - to end everything right then and there'
    print 'bag - to view your inventroy'
    print 'exit - to exit the game'


def welcome():
    print ''
    print 'Welcome to the 8-bit Federation Adventure test.'
    print 'At any moment in the game type in \'help\' for the list of commands.'

welcome()
while True:
    print ''
    look()
    cmd = requestString("Type an action, or \'help\' ")
    if cmd is None:
      continue
      
    cmd = cmd.lower()

    if cmd == 'look':
        look()
    elif cmd in ['w', 'west', 'e', 'east', 's', 'south', 'n', 'north']:
        # only send the first letter
        move(cmd[:1])
    elif cmd.startswith('take'):
        # remove take if it's there and strip whitespace
        item = cmd.split('take', 1)[-1].strip()
        take(item)
    elif cmd == 'bag':
        bag()
    elif cmd == 'help':
        help()
    elif cmd == 'exit':
        print "Thanks for playing."
        break
    elif cmd == 'kill':
        print 'You have lost the will to continue on and moved into the netherworld...'
        print 'Game Over!'
        break
    else:
        print 'You are lost, confused, and do not know what to do...'
